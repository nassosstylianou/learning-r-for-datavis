## Learning R for datavis
Link: http://learning-r-for-datavis.netlify.com/ 

## What's this?
This repo includes all the code that powers the `Learning R for datavis` website that BBC's Visual Journalism data team have put together in order to help colleagues in the Vis Jo and other BBC teams learn the statistical programming language R to create production-ready graphics for the BBC News site. 

## What's the plan?
The idea is that the website will act as a resource for visual and data journalists, designers, developers and anyone else interested to go through a six-week syllabus. 

Draft outline of the syllabus:
http://learning-r-for-datavis.netlify.com/blog/2018-05-29-make-bbc-style-graphics-in-six-weeks/

By the end of the syllabus, the ambition is that that those following will reach the point where they can produce graphics in a BBC style and follow the `R Cookbook` that the data team has put together. 
https://github.com/bbc/vjdata.rcookbook 


## How to contribute to the site 
There is the [basic outline for the entire syllabus](http://learning-r-for-datavis.netlify.com/blog/2018-05-29-make-bbc-style-graphics-in-six-weeks/), but the plan is for each week to have a slightly more in-depth blog post and guidelines, which will be prepared and published to the site the week before. It should also include  the details and times that the data team members leading the course be available. 

The way you can contribute to the site and write up blog posts is by cloning this repo. 
https://bitbucket.org/nassosstylianou/learning-r-for-datavis

It is on `Bitbucket` and not `Github` as is our standard workflow so that we can have it run a public site, which would not be possible in the current workflow to use `Github` given the permissions the repo needs. 

You will need to create a `Bitbucket` account as it will ask you for Bitbucket credentials when pushing, but that should be really straightforward and the repo is public anyway. 

### Cloning the repo
But the only thing you need to do differently is run this command in your terminal:
`git clone https://bitbucket.org/nassosstylianou/learning-r-for-datavis.git`

Once you've done this, you should be able to open the `.Rproj` file in the folder you have just cloned, which should open up the project with the files running the site. 

### The blogdown package
To make changes, new posts and run the blog you will have to first install the `blogdown` package.

To run the blog for the first time, you will also have to install `Hugo`, which is the framework that `blogdown` uses for building the site and also includes a number of themes, including the one we are using.

To install `hugo` you just need to run `blogdown::install_hugo()`.

### Running the site
Once you have have done this, to actually serve the site locally, you need to run `blogdown::serve_site()`.

The site will then appear in your `R Studio` session in your viewer window on the bottom right. If you click on the little 'Show in New Window' icon at the toolbar of your viewer box, it will open up the site in your browser window. 

The site should then update locally when you make any changes, because when you save files it re-compiles. 

### Authoring a new post
You can create a new blog post by running
`blogdown::new_post()` or you can use the `Addins` menu. 

When you installed the `blogdown` package, it should have also given you access to some `Addins` which you can find by clicking the `Addins` button on the top of the menubar. One of the options is New Post and clicking that will create a new post. 

### Pushing changes to the live site
Once you have made your changes and the local version of the site is as you want it, the way to make your changes public is to just push the updates to the live bitbucket repository, in the same way you would update a `Github` repo, as long as you have cloned the repo using the command above. 

Note: You will need to be added as a collaborator to be able to push updates. 
