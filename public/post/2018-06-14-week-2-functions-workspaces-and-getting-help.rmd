---
title: "Week 2: Functions, workspaces and getting help"
author: "Clara"
date: "2018-06-14T15:12:11+01:00"
categories: ["R"]
tags: ["R Markdown", "ggplot2", "R"]
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(collapse = TRUE)
```

Before you start this week you should have R and RStudio up and running on your laptop. 

## What will we cover this week?

This week we’ll start familiarising ourselves with working with R on our computers. The ComputerWorld tutorial [Beginner’s Guide to R](https://www.computerworld.com/article/2497143/business-intelligence/business-intelligence-beginner-s-guide-to-r-introduction.html) introduces RStudio, what a working directory is (and how to change it), and most importantly perhaps the most useful function of all: `help()` or `?`.

You can use it to master any other R function.

If you are unsure about how a function works either of these commands will open up the documentation right in your RStudio window. This will include a description of the functions and its arguments as well as a few examples at the end. 

If you’re thinking “Okay, but what’s a function?”, fear not: the chapter “Building Blocks” in the book R for Excel Users introduces functions and takes us from individual data points to working with data frames (R lingo for a spreadsheet). 

Finally, the Datacamp article [R packages: A beginner’s guide](https://www.datacamp.com/community/tutorials/r-packages-guide) introduces the concept of extending the power of R with something called packages. 

Packages are like add-ons that extend the power of R - you can think of them a little like Chrome extensions. We will be introducing a few specific packages in the weeks to come including dplyr for data manipulation and of course ggplot2 to create all our graphs. 

For this week, it’s enough to know that packages exist and how you might go about installing and loading a new package. 

To summarise, by the end of week two you should:    
* Know what a function is     
* Be able to figure out what working directory you are in and navigate to a different one    
* Install an R package and load it into your current R session    
* Use the `help()` or `?` function to find more information about an R package or function    
 
## Learning material

* [Beginner’s Guide to R: Part 1](https://www.computerworld.com/article/2497143/business-intelligence/business-intelligence-beginner-s-guide-to-r-introduction.html) (you only need to read part 1!)  
* R For Excel Users: Part 2 - Building Blocks: Cells and Formulas (p.29-51)  
* [R packages: A beginner’s guide](https://www.datacamp.com/community/tutorials/r-packages-guide)  

Additionally, Datacamp has a [“Practice R” setting](https://challenges.datacamp.com/practice/106) which is good for practice and repetition.

## Exercises

1. How do you set your working directory in R?

2. Bonus question: How can you check which working directory you’re in? [N.B. This will require some googling!]

3. What would you type into your console to find out more about the function `nchar`? 

4. How can you check what R packages you have installed on your computer?

5. Working with `ages <- c(2, 5, 10, 94, 3)` (type it into your console), find the:
  * Maximum value 
  * Mean value
  * Sum
