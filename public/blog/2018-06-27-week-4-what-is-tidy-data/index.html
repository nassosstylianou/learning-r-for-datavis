<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="generator" content="Hugo 0.41" />
		<title>Week 4: What Is Tidy Data? - Learning R for data visualisation</title>

		<meta name="description" content="Before starting this week, you should have had a go at loading a dataset into R and examining it using functions like str(), glimpse() and head().
What will we cover this week? This week things start to get really exciting: now that we’ve got some practice at importing our data, we’re going to start learning how to manipulate it.
Often you need to work with your data and make some changes to it to prepare it for visualisation.">


		
		
		
		<link rel="stylesheet" href="/css/ui.min.css"/>
		
		
		

		<link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Mono|Lato|Raleway">

		

		<style>
	a { color: #ff8181; }
	blockquote {
		border-left-color: #ff8181;
		border-right-color: #ff8181; }
	.bar a:hover {
		color: #ff8181;
		text-decoration: none; }
	.sep {
		margin-top: 2rem;
		margin-bottom: 1rem;
		margin-left:0;
		width: 24rem;
		border-top: 2px solid #ff8181; }
</style>

	</head>

<body>
<header class="container no-print">
	<div class="u-header">
		<nav class="bar">
	<ul><li>
			<a href="/">
				<img class="icon-text" src="/img/prev.svg"/>
			</a>
		</li><li><a href="/about/">About</a></li></ul>
</nav>

	</div>
</header>
<main class="container">

<article>
	<header><hgroup id="brand">
	<h1>Week 4: What Is Tidy Data?</h1>
	<h5>
		
		<time datetime="2018-06-27 15:52:39 &#43;0100 BST">Jun 27, 2018</time>
		<span class="no-print">
			-
				
				<a href="/tags/R%20Markdown">R Markdown</a>
				
				<a href="/tags/ggplot2">ggplot2</a>
				
				<a href="/tags/R">R</a>
				<span>
	</h5>
	
</hgroup>
<hr class="sep" />
</header>
	<p>Before starting this week, you should have had a go at loading a dataset into R and examining it using functions like <code>str()</code>, <code>glimpse()</code> and <code>head()</code>.</p>
<div id="what-will-we-cover-this-week" class="section level2">
<h2>What will we cover this week?</h2>
<p>This week things start to get really exciting: now that we’ve got some practice at importing our data, we’re going to start learning how to manipulate it.</p>
<p>Often you need to work with your data and make some changes to it to prepare it for visualisation.</p>
<p>You may already be used to doing this in Excel: adding a column that calculates percentages, for instance, or cleaning up inconsistent names.</p>
<p>Learning the basics of working with data directly in R will make it easier to make changes to your data when in the process of making a chart.</p>
<p>This week we will start by installing the <code>tidyr</code> package, which helps us work with our data.</p>
<p>Read <a href="http://blog.rstudio.com/2014/07/22/introducing-tidyr/">Introducing tidyr</a>, which introduces the concept of “tidy data”, as well as the difference between “long” and “wide” data</p>
<p>First things first: what is tidy data? It’s when your dataset is arranged in this format:</p>
<ul>
<li>Each variable is a column</li>
<li>Each observation, or case, is a row</li>
</ul>
<p>Tidy datasets have many advantages: they’re easier to work with, to manipulate and to make visualisations with.</p>
<p>Wide data is when our dataset has multiple observations on each row. Consider, for instance, this (entirely made up) dataset showing life expectancy in a number of countries:</p>
<p><img src="/img/week4_wide.png" /><!-- --></p>
<p>Columns B, C, and D are actually values themselves, not variables, as they represent years. Each row in this example actually contains three observations: one for every year.</p>
<p>This is the same data, but in a long and tidy format:</p>
<p><img src="/img/week4_long.png" /><!-- --></p>
<p>Each observation is now on a separate row, and we have a column for each of the variables in our dataset: country, life expectancy and year.</p>
<p>It can be a little tricky to wrap your head around the difference between long and wide data, but there is a helpful explanation in <a href="https://data.library.virginia.edu/a-tidyr-tutorial/">A tidyr Tutorial</a>. Don’t worry if it doesn’t make sense, and as always, do give us a shout or ask in the Slack channel if you have any questions!</p>
<p>Often datasets that we work with are in a wide format, because it’s more convenient for data entry. But almost all analysis and visualisation we do in R requires data in a long format.</p>
<p>So how can we go about reshaping our data?</p>
<p>There are two useful functions in the tidyr package for this exact purpose:</p>
<ul>
<li><code>gather()</code>, which takes wide data and makes it long</li>
<li><code>spread()</code>, which takes long data and makes it wide</li>
</ul>
<p>Another useful function is <code>separate()</code>, which allows us to split the contents of one column into two.</p>
<p>By the end of this week you should:</p>
<ul>
<li>Know the difference between long and wide data</li>
<li>Understand how to convert your data from one form to another</li>
<li>Separate the contents of one column</li>
</ul>
<p>Next week we’ll expand on manipulating data with the dplyr package.</p>
</div>
<div id="learning-material" class="section level2">
<h2>Learning material:</h2>
<ul>
<li><a href="http://blog.rstudio.com/2014/07/22/introducing-tidyr/">Introducing tidyr</a></li>
<li><a href="https://data.library.virginia.edu/a-tidyr-tutorial/">A tidyr Tutorial</a></li>
</ul>
<p>If you found Swirl useful last week, good news: There is a special Swirl course for tidyr.</p>
<p>To install it, type the following into your console:</p>
<pre class="r"><code>install.packages(“swirl”) #Only run this line if you haven’t already installed the swirl package

library(swirl)
install_course(&quot;Getting and Cleaning Data&quot;)
swirl()</code></pre>
<p>You should now be able to choose between the default R Programming course and Getting and Cleaning Data when swirl loads. Select the latter and then choose the lesson “3. Tidying Data with tidyr” to get started.</p>
</div>
<div id="exercises" class="section level2">
<h2>Exercises:</h2>
<ol style="list-style-type: decimal">
<li><p>Examine the dataset <code>USArrests</code> (which comes pre-loaded with R) with functions we learned last week, e.g. <code>head()</code>, <code>str()</code> and View().</p></li>
<li><p>Notice the dataset doesn’t have a column for states: that’s because these are listed as the rownames instead. Let’s create a column containing the states by running the following command: <code>USArrests$State &lt;- rownames(USArrests)</code></p></li>
<li><p>Now gather the data for murder, assault and rape, making sure to exclude the columns State and UrbanPop. Save the results as a new data frame, <code>arrests_long</code>.</p></li>
<li><p>Let’s put the data back the way it was. Spread the data you saved as <code>arrests_long</code> back to a wide format, and save this as <code>arrests_wide</code>.</p></li>
</ol>
</div>

</article>
<nav class="no-print post-nav">

	<a class="prev-post" href="/blog/2018-06-20-week-3-importing-data-into-r/">
		<img class="icon-text" src="/img/prev.svg"/>Week 3: Importing Data Into R</a>


	<a class="next-post" href="/blog/2018-07-04-week-4-bonus-clara-s-tidyr-tutorial/">Week 4 Bonus: Clara&#39;s Tidyr Tutorial<img class="icon-text" src="/img/next.svg"/>
	</a>

</nav>


<section id="related">
  <h4>See Also</h4>
  <ul>
    
  	<li><a href="/blog/2018-06-20-week-3-importing-data-into-r/">Week 3: Importing Data Into R</a></li>
  	

  	<li><a href="/blog/2018-06-14-week-2-functions-workspaces-and-getting-help/">Week 2: Functions, workspaces and getting help</a></li>
  	
  </ul>
</section>



	<div id="disqus_thread" class="no-print"></div>
<script type="text/javascript">

(function() {
    
    
    if (window.location.hostname == "localhost")
        return;

    var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
    var disqus_shortname = '';
    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>


			<hr class="sep" />
		</main>
		<footer class="container no-print">
			<div class="u-footer">
				
<a href="mailto:nassos.stylianou@bbc.co.uk"><img class="icon-social" src="/img/email.svg" alt="Email Me!"/></a>


<a href="https://github.com/bbc/vjdata.rcookbook"><img class="icon-social" src="/img/github.svg" alt="Github"/></a>


<a href="https://twitter.com/BBCNewsgraphics"><img class="icon-social" src="/img/twitter.svg" alt="Twitter"/></a>

<a href="/index.xml" target="_blank"><img class="icon-social" src="/img/feed.svg" alt="Feed"></a>

				<p>
					
					Theme used: <a href="https://github.com/yursan9/manis-hugo-theme">Manis</a><br>
					
					
					&copy; 2018 Yurizal Susanto
					
					
					| <a href="/about/license">License</a>
					
				</p>
				
				<a href="#brand">
					<img class="icon-text" src="/img/toup.svg" alt="To Up"/>
					<span>Back to Up</span>
				</a>
				
			</div>
		</footer>
		
	</body>
</html>

