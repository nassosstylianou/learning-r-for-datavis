+++
title = "About"
date = "2018-05-24T18:57:12+07:00"
menu = "main"
+++
This is a website built by the BBC Visual Journalism Data team that includes resources and tutorials on learning `R` to be able to create BBC-style graphics.
