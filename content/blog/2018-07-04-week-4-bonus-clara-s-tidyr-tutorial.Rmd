---
title: "Week 4 Bonus: Clara's Tidyr Tutorial"
author: "Clara"
date: 2018-07-03T11:23:34+01:00
categories: ["R"]
tags: ["R Markdown", "ggplot2", "R"]
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

We're going to go through the `tidyr` functions `gather()` and `spread()` together using a dataset on life expectancy.

## Let's get the data

We're going to be working with a global health dataset called `gapminder` that you can get into your RStudio workspace by installing and loading the `gapminder` package. 

```{r}
# install.packages("gapminder")
library(gapminder)
```

We'll also need to load `tidyr`:

```{r}
library(tidyr)
```

The gapminder dataset should now be in your workspace. Let's assign it to a variable called `data`:

```{r}
data <- gapminder
```

Time to have a look at our data. 

Let's check it out using some useful summary functions, like `str()` or `head()`:

```{r}
head(data)
```

The data shows life expectancy, population and GDP figures for every country. 

Also, crucially for the purposes of our exercise: we can see it's in tidy format! Each year is on a different row. 

## Making long data wide

But what if we want to find out the **percentage change in life expectancy between 1952 and 2007**?

We'll need to make the data wide, with a column for every year. Enter `spread()`. 

### 1: Subsetting the data

We only need a few columns of the dataset: the columns containing GDP and population are superfluous for this analysis. 

Let's create a new variable called `data_lifeExp` which selects the first 4 columns of data: 

```{r}
data_lifeExp <- data[,1:4]
```

### 2: Making the data wide

We need to make the data wide to be able to calculate the percentage change between different years. 

Use `spread()` on `data_lifeExp`, using year and lifeExp as the **key-value pair**. 

(The **key** is what you want your new columns to be called, and the **value** is the observation you want those columns to contain.)

```{r}
data_wide <- spread(data_lifeExp, key = year, value = lifeExp)
```

N.B. You don't have to name the arguments explicitly, if you do it in the same order. The below will work too:

```{r}
data_wide <- spread(data_lifeExp, year, lifeExp)
```

### 3: Calculating percentage change

Our data is in the right format! Time to add a column calculating the percentage change. 

```{r}
data_wide$perc_change <- 100*(data_wide$`2007` - data_wide$`1952`)/data_wide$`1952`
```

Note:

* Select the columns you want to work with with a `$`
* Remember that to select columns with numerical names (like the years) you need to wrap them in \`backquotes\`

Your dataset should now have 15 columns instead of 14. Have a look at it.

```{r eval = FALSE}
View(data_wide)
```

Our percentage change column is a little messy. Let's round the numbers instead:

```{r}
data_wide$perc_change <- round(data_wide$perc_change, 1)
```

Nice! 

...now, let's undo all our good work.

## Making wide data long

Our goal is to get the data back in the same nice long format as it was, using `gather()`. 

The key-value pair is the same, but note that you must specify which columns you don't want to gather with a minus sign. 

```{r}
data_long <- gather(data_wide[,1:14], key = year, value = lifeExp, -country, -continent)
```

This code ignores the percentage change column we just created by selecting only the first 14 columns of our dataset.

### Testing with identical()

Our data frame `data_long` should be identical to the original dataset, `data_lifeExp`. Let's test it!

```{r eval = FALSE}
identical(data_long, data_lifeExp)
```

