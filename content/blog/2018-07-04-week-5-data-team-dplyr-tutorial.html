---
title: "Week 5 Bonus: Nassos' Dplyr Tutorial"
author: "Nassos"
date: 2018-07-04T11:23:34+01:00
categories: ["R"]
tags: ["R Markdown", "ggplot2", "R"]
---



<p>Only come to this once you have gone through the main week 5 <code>dplyr</code> tutorial first.</p>
<p><a href="http://learning-r-for-datavis.netlify.com/blog/2018-07-04-week-5-manipulating-data-with-dplyr" class="uri">http://learning-r-for-datavis.netlify.com/blog/2018-07-04-week-5-manipulating-data-with-dplyr</a></p>
<p>The dataset for this tutorial is saved in Dropbox and you should be able to download it here: <a href="https://www.dropbox.com/s/60lor20tvp5luq0/ward_data_dplyr_training.csv?dl=0" class="uri">https://www.dropbox.com/s/60lor20tvp5luq0/ward_data_dplyr_training.csv?dl=0</a></p>
<p>It should also be attached to your week 5 lesson email as well.</p>
<p>You should already know how the pipe <code>%&gt;%</code> operator works as well as what the main <code>dplyr</code> verbs are, so we will look at each one in detail here.</p>
<div id="load-in-the-data" class="section level4">
<h4>Load in the data</h4>
<p>We will be using the data we analysed for the <a href="http://www.bbc.co.uk/news/business-41582755">ward-level house price project</a>. We can look at what each column of the data is using <code>dplyr</code>’s <code>glimpse()</code> function, which is like a transposed version of print but shows you your data in a readable format in your R console.</p>
<pre class="r"><code>ward_data &lt;- read.csv(url(&#39;https://www.dropbox.com/s/60lor20tvp5luq0/ward_data_dplyr_training.csv?dl=1&#39;))</code></pre>
</div>
<div id="select" class="section level4">
<h4>Select</h4>
<p>The select function is pretty straightforward - it selects the columns that you want, dropping any other columns in the data. For example, if you look at our data, by running the <code>str()</code> function on our data to view its structure, you can see that there are 16 columns, only some of which we need to continue with our analysis.</p>
<p><code>select(data, column1name, column2name, column3name)</code></p>
<pre class="r"><code>ward_data_select &lt;- select(ward_data, ward_name, local_authority, region, X2007, X2017)</code></pre>
<table style="width:93%;">
<colgroup>
<col width="23%" />
<col width="31%" />
<col width="12%" />
<col width="12%" />
<col width="12%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">ward_name</th>
<th align="left">local_authority</th>
<th align="left">region</th>
<th align="left">X2007</th>
<th align="left">X2017</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">Abbey</td>
<td align="left">Barking and Dagenham</td>
<td align="left">London</td>
<td align="left">166496</td>
<td align="left">230000</td>
</tr>
<tr class="even">
<td align="left">Alibon</td>
<td align="left">Barking and Dagenham</td>
<td align="left">London</td>
<td align="left">183000</td>
<td align="left">294000</td>
</tr>
<tr class="odd">
<td align="left">Becontree</td>
<td align="left">Barking and Dagenham</td>
<td align="left">London</td>
<td align="left">190000</td>
<td align="left">300000</td>
</tr>
<tr class="even">
<td align="left">Chadwell Heath</td>
<td align="left">Barking and Dagenham</td>
<td align="left">London</td>
<td align="left">215000</td>
<td align="left">340000</td>
</tr>
<tr class="odd">
<td align="left">Eastbrook</td>
<td align="left">Barking and Dagenham</td>
<td align="left">London</td>
<td align="left">210000</td>
<td align="left">330000</td>
</tr>
<tr class="even">
<td align="left">Eastbury</td>
<td align="left">Barking and Dagenham</td>
<td align="left">London</td>
<td align="left">186500</td>
<td align="left">318750</td>
</tr>
</tbody>
</table>
<p>This is great but imagine if you want to select many columns, it’s not ideal to type out every single one. There are other ways to select columns other than to specifically write all the column names out.</p>
<p><code>select(ward_data, 1:2, starts_with(&quot;X&quot;))</code></p>
<p><code>Dplyr</code> comes with a set of helper functions that can help you select groups of variables inside a <code>select()</code> call. Here are some examples that may come in handy:</p>
<ul>
<li><code>starts_with(&quot;X&quot;)</code>: every name that starts with “X”,</li>
<li><code>ends_with(&quot;X&quot;)</code>: every name that ends with “X”,</li>
<li><code>contains(&quot;X&quot;)</code>: every name that contains “X”,</li>
<li><code>matches(&quot;X&quot;)</code>: every name that matches “X”, where “X” can be a regular expression,</li>
<li><code>one_of(x)</code>: every name that appears in x, which should be a character vector.</li>
</ul>
</div>
<div id="mutate" class="section level4">
<h4>Mutate</h4>
<p><code>Mutate()</code> creates new columns based on calculations using the existing columns, with the new columns added to the dataset.</p>
<p>Below, we will use mutate to create a new column in our dataset that calculates the percentage change in the median house price for each ward from 2007 to 2017 .</p>
<pre class="r"><code>ward_data_mutate &lt;- mutate(ward_data_select, percentage_change = ((X2017 - X2007) / X2007) * 100)
## Warning: package &#39;bindrcpp&#39; was built under R version 3.4.4</code></pre>
<p>You can add multiple variables to the same <code>mutate()</code> function, for example create two new columns in one <code>mutate()</code> call. Below, we are calculating the real price change from 2007 to 2017 as well as doing a rough inflation adjusted percentage change. We are separating the two new columns we are creating with a <code>,</code>.</p>
<pre class="r"><code>ward_data_mutate &lt;- mutate(ward_data_mutate, 
                           price_diff = X2017 - X2007, 
                           X2007_inf = X2007 * 1.22,
                           perc_change_inf_adjusted = (X2017 - X2007_inf) / X2007_inf * 100)</code></pre>
<table style="width:93%;">
<caption>Table continues below</caption>
<colgroup>
<col width="23%" />
<col width="31%" />
<col width="12%" />
<col width="12%" />
<col width="12%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">ward_name</th>
<th align="left">local_authority</th>
<th align="left">region</th>
<th align="left">X2007</th>
<th align="left">X2017</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">Abbey</td>
<td align="left">Barking and Dagenham</td>
<td align="left">London</td>
<td align="left">166496</td>
<td align="left">230000</td>
</tr>
<tr class="even">
<td align="left">Alibon</td>
<td align="left">Barking and Dagenham</td>
<td align="left">London</td>
<td align="left">183000</td>
<td align="left">294000</td>
</tr>
<tr class="odd">
<td align="left">Becontree</td>
<td align="left">Barking and Dagenham</td>
<td align="left">London</td>
<td align="left">190000</td>
<td align="left">300000</td>
</tr>
<tr class="even">
<td align="left">Chadwell Heath</td>
<td align="left">Barking and Dagenham</td>
<td align="left">London</td>
<td align="left">215000</td>
<td align="left">340000</td>
</tr>
<tr class="odd">
<td align="left">Eastbrook</td>
<td align="left">Barking and Dagenham</td>
<td align="left">London</td>
<td align="left">210000</td>
<td align="left">330000</td>
</tr>
<tr class="even">
<td align="left">Eastbury</td>
<td align="left">Barking and Dagenham</td>
<td align="left">London</td>
<td align="left">186500</td>
<td align="left">318750</td>
</tr>
</tbody>
</table>
<table style="width:99%;">
<colgroup>
<col width="27%" />
<col width="18%" />
<col width="16%" />
<col width="36%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">percentage_change</th>
<th align="left">price_diff</th>
<th align="left">X2007_inf</th>
<th align="left">perc_change_inf_adjusted</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">38.14</td>
<td align="left">63504</td>
<td align="left">203125</td>
<td align="left">13.23</td>
</tr>
<tr class="even">
<td align="left">60.66</td>
<td align="left">111000</td>
<td align="left">223260</td>
<td align="left">31.69</td>
</tr>
<tr class="odd">
<td align="left">57.89</td>
<td align="left">110000</td>
<td align="left">231800</td>
<td align="left">29.42</td>
</tr>
<tr class="even">
<td align="left">58.14</td>
<td align="left">125000</td>
<td align="left">262300</td>
<td align="left">29.62</td>
</tr>
<tr class="odd">
<td align="left">57.14</td>
<td align="left">120000</td>
<td align="left">256200</td>
<td align="left">28.81</td>
</tr>
<tr class="even">
<td align="left">70.91</td>
<td align="left">132250</td>
<td align="left">227530</td>
<td align="left">40.09</td>
</tr>
</tbody>
</table>
</div>
<div id="filter" class="section level4">
<h4>Filter</h4>
<p>With <code>filter()</code>, we can filter/select the observations (rows of data) we want, based on their value. Filter works well uisng the logical operators in R.</p>
<p><code>filter(data, logical test)</code></p>
<p>Here are some of the logical operators that you can use inside <code>filter()</code>:</p>
<ul>
<li><code>x &lt; y</code>, TRUE if x is less than y</li>
<li><code>x &lt;= y</code>, TRUE if x is less than or equal to y</li>
<li><code>x == y</code>, TRUE if x equals y</li>
<li><code>x != y</code>, TRUE if x does not equal y</li>
<li><code>x &gt;= y</code>, TRUE if x is greater than or equal to y</li>
<li><code>x &gt; y</code>, TRUE if x is greater than y</li>
<li><code>x %in% c(a, b, c)</code>, TRUE if x is in the vector c(a, b, c)</li>
</ul>
<p>In our case, we know that if any ward had fewer than 10 transactions in a particular year, we did not calculate a median for it for that year. For the purposes of this training, in our data that is indicated by a 0 in that year’s column. So first up, we want to remove those rows from our data.</p>
<pre class="r"><code>ward_data_filter &lt;- filter(ward_data_mutate, X2007 != 0, X2017 != 0)</code></pre>
<p>We can use commas to separate the different filters, or we can also use R’s <code>&amp;</code> (and), <code>|</code> (or) and <code>!</code> (not) operators.</p>
<p>Also worth looking into are the <code>any()</code> and <code>all()</code> functions as a cleaner way to have multiple <code>&amp;</code> or <code>|</code> arguments.</p>
<p><code>any()</code> works like the or <code>|</code> function, while <code>all()</code> works like <code>&amp;</code>.</p>
<p>Say we want to find the wards that recorded more than just a minor drop in the median house price from 2007 to 2017. We decide that this should be the wards with a percentage change decline higher than 10% and a price drop of more than £10,000.</p>
<pre class="r"><code>ward_data_major_decrease &lt;- filter(ward_data_filter, price_diff &lt;= -10000 &amp; percentage_change &lt; -10)
summary(ward_data_major_decrease)
##    ward_name          local_authority                      region   
##  Castle :  2   County Durham  : 24    WALES                   :148  
##  Central:  2   Pembrokeshire  : 21    North West              :130  
##  Croft  :  2   Carmarthenshire: 18    North East              : 94  
##  Everton:  2   Gwynedd        : 18    Yorkshire and The Humber: 51  
##  Graig  :  2   Northumberland : 18    South West              : 32  
##  Morton :  2   Ceredigion     : 13    East Midlands           : 28  
##  (Other):505   (Other)        :405    (Other)                 : 34  
##      X2007            X2017        percentage_change   price_diff     
##  Min.   : 60000   Min.   : 36000   Min.   :-54.55    Min.   :-174005  
##  1st Qu.:102000   1st Qu.: 81000   1st Qu.:-22.39    1st Qu.: -35000  
##  Median :132500   Median :109000   Median :-17.00    Median : -23002  
##  Mean   :161550   Mean   :131274   Mean   :-18.83    Mean   : -30276  
##  3rd Qu.:197000   3rd Qu.:163000   3rd Qu.:-12.82    3rd Qu.: -17000  
##  Max.   :680000   Max.   :556500   Max.   :-10.02    Max.   : -10000  
##                                                                       
##    X2007_inf      perc_change_inf_adjusted
##  Min.   : 73200   Min.   :-62.74          
##  1st Qu.:124440   1st Qu.:-36.38          
##  Median :161650   Median :-31.97          
##  Mean   :197091   Mean   :-33.46          
##  3rd Qu.:240340   3rd Qu.:-28.54          
##  Max.   :829600   Max.   :-26.25          
## </code></pre>
<p>That leaves us with 517 wards where the raw price dropped by at least £10,000 and the percentage decrease from 2007 to 2017 was more than 10%.</p>
<p>So for now, we want to end up with the wards that have decreased in inflation-adjusted terms, which we can do with the following <code>filter()</code> command.</p>
<pre class="r"><code>ward_data_decrease &lt;- filter(ward_data_filter, perc_change_inf_adjusted &lt; 0)</code></pre>
<table style="width:90%;">
<caption>Table continues below</caption>
<colgroup>
<col width="22%" />
<col width="25%" />
<col width="18%" />
<col width="12%" />
<col width="12%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">ward_name</th>
<th align="left">local_authority</th>
<th align="left">region</th>
<th align="left">X2007</th>
<th align="left">X2017</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">Cockfosters</td>
<td align="left">Enfield</td>
<td align="left">London</td>
<td align="left">390000</td>
<td align="left">472500</td>
</tr>
<tr class="even">
<td align="left">Cranford</td>
<td align="left">Hounslow</td>
<td align="left">London</td>
<td align="left">249000</td>
<td align="left">295000</td>
</tr>
<tr class="odd">
<td align="left">Bishop’s</td>
<td align="left">Lambeth</td>
<td align="left">London</td>
<td align="left">369950</td>
<td align="left">350000</td>
</tr>
<tr class="even">
<td align="left">Astley Bridge</td>
<td align="left">Bolton</td>
<td align="left">North West</td>
<td align="left">142500</td>
<td align="left">150000</td>
</tr>
<tr class="odd">
<td align="left">Bradshaw</td>
<td align="left">Bolton</td>
<td align="left">North West</td>
<td align="left">164000</td>
<td align="left">170500</td>
</tr>
<tr class="even">
<td align="left">Breightmet</td>
<td align="left">Bolton</td>
<td align="left">North West</td>
<td align="left">105000</td>
<td align="left">99000</td>
</tr>
</tbody>
</table>
<table style="width:99%;">
<colgroup>
<col width="27%" />
<col width="18%" />
<col width="16%" />
<col width="36%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">percentage_change</th>
<th align="left">price_diff</th>
<th align="left">X2007_inf</th>
<th align="left">perc_change_inf_adjusted</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">21.15</td>
<td align="left">82500</td>
<td align="left">475800</td>
<td align="left">-0.6936</td>
</tr>
<tr class="even">
<td align="left">18.47</td>
<td align="left">46000</td>
<td align="left">303780</td>
<td align="left">-2.89</td>
</tr>
<tr class="odd">
<td align="left">-5.393</td>
<td align="left">-19950</td>
<td align="left">451339</td>
<td align="left">-22.45</td>
</tr>
<tr class="even">
<td align="left">5.263</td>
<td align="left">7500</td>
<td align="left">173850</td>
<td align="left">-13.72</td>
</tr>
<tr class="odd">
<td align="left">3.963</td>
<td align="left">6500</td>
<td align="left">200080</td>
<td align="left">-14.78</td>
</tr>
<tr class="even">
<td align="left">-5.714</td>
<td align="left">-6000</td>
<td align="left">128100</td>
<td align="left">-22.72</td>
</tr>
</tbody>
</table>
</div>
<div id="arrange" class="section level4">
<h4>Arrange</h4>
<p>The <code>arrange()</code> verb is pretty straightforward and the basic premise is simple: it can be used to re-arrange rows according to any type of data, not just numeric data. If you pass <code>arrange()</code> a character variable, it will arrange the rows in alphabetical order according to values of the variable.</p>
<p>By default, arrange() orders the rows from smallest to largest. Rows with the smallest value will go to the top of the column. In order to reverse this, you can wrap your variable name in the <code>desc()</code> function it the <code>arrange()</code>, for example <code>arrange(data, desc(column_name))</code>.</p>
<p>In our case, we want to arrange our data in order from smallest to largest by inflation adjustment percentage change, so that the wards with the largest decrease in median house price will be at the top.</p>
<pre class="r"><code>ward_data_decrease_arrange &lt;- arrange(ward_data_decrease, perc_change_inf_adjusted)</code></pre>
<table style="width:100%;">
<colgroup>
<col width="19%" />
<col width="28%" />
<col width="19%" />
<col width="32%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">ward_name</th>
<th align="left">local_authority</th>
<th align="left">region</th>
<th align="left">perc_change_inf_adjusted</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">Aberdulais</td>
<td align="left">Neath Port Talbot</td>
<td align="left">WALES</td>
<td align="left">-62.74</td>
</tr>
<tr class="even">
<td align="left">Riverside</td>
<td align="left">Liverpool</td>
<td align="left">North West</td>
<td align="left">-59.88</td>
</tr>
<tr class="odd">
<td align="left">North Ormesby</td>
<td align="left">Middlesbrough</td>
<td align="left">North East</td>
<td align="left">-57.85</td>
</tr>
<tr class="even">
<td align="left">Aberdovey</td>
<td align="left">Gwynedd</td>
<td align="left">WALES</td>
<td align="left">-55.83</td>
</tr>
<tr class="odd">
<td align="left">Horden</td>
<td align="left">County Durham</td>
<td align="left">North East</td>
<td align="left">-55.75</td>
</tr>
<tr class="even">
<td align="left">Town</td>
<td align="left">Newcastle-under-Lyme</td>
<td align="left">West Midlands</td>
<td align="left">-55.6</td>
</tr>
</tbody>
</table>
<p>You can also sort on multiple columns in R,<code>arrange()</code> also lets you add a second variable name to sort with after the first one.</p>
</div>
<div id="summarise" class="section level4">
<h4>Summarise</h4>
<p>The last of the five <code>dplyr</code> verbs, <code>summarise()</code>, follows the same syntax as <code>mutate()</code>. However the result of running a summarise call is a new dataset that consists of a single row and not an entire new column in the data.</p>
<p>You can use a range of functions in <code>summarise()</code>, just as long as it can take a vector of data and give back a single number - functions that are termed ‘aggregating functions’.</p>
<p>Here are a few of them (combination of base <code>R</code> and <code>dplyr</code> ones):</p>
<ul>
<li><code>min(x)</code> - minimum value of vector x.</li>
<li><code>max(x)</code> - maximum value of vector x.</li>
<li><code>mean(x)</code> - mean value of vector x.</li>
<li><code>median(x)</code> - median value of vector x.</li>
<li><code>sd(x)</code> - standard deviation of vector x.</li>
<li><code>diff(range(x))</code> - total range of vector x.</li>
<li><code>first(x)</code> - The first element of vector x.</li>
<li><code>last(x)</code> - The last element of vector x.</li>
<li><code>nth(x, n)</code> - The nth element of vector x.</li>
<li><code>n()</code> - The number of rows in the data.frame or group of observations that <code>summarise()</code> describes.</li>
<li><code>n_distinct(x)</code> - The number of unique values in vector x.</li>
</ul>
<p>So for example, if we want to find how many different local authorities we have in our entire dataset, we can do so using <code>summarise()</code> and <code>n_distinct(x)</code>.</p>
<pre class="r"><code>summarise(ward_data_decrease_arrange, number_of_regions = n_distinct(region))
##   number_of_regions
## 1                10</code></pre>
<p>We can also find out the minimum and maximum average house price:</p>
<pre class="r"><code>summarise(ward_data_decrease_arrange, minimum_price_07 = min(X2007), maximum_price_07 = max(X2017))
##   minimum_price_07 maximum_price_07
## 1            53463           760000</code></pre>
</div>
<div id="group-by-group_by" class="section level4">
<h4>Group by (group_by)</h4>
<p>Now this is where <code>dplyr</code> really comes into its own and becomes extremely useful - when you group your data, and use <code>summarise()</code> functions on the grouped datasets to find .</p>
<p>What <code>group_by()</code> does is allow you to define groups within your data set. The power of doing that becomes clear when calling <code>mutate()</code> or <code>summarise()</code> on a dataset that you have grouped. Doing that means that summarising statistics are calculated for the different groups separately.</p>
<p>For example, from our house price data filtered on the wards where the inflation-adjusted average house price has gone down in the 10 years to 2017, we want to get a breakdown of those wards by region.</p>
<p>The first thing we need to do is group our dataset by region.</p>
<pre class="r"><code>ward_data_decrease_grouped &lt;- group_by(ward_data_decrease_arrange, region)</code></pre>
<table>
<caption>Table continues below</caption>
<colgroup>
<col width="21%" />
<col width="31%" />
<col width="21%" />
<col width="12%" />
<col width="12%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">ward_name</th>
<th align="left">local_authority</th>
<th align="left">region</th>
<th align="left">X2007</th>
<th align="left">X2017</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">Aberdulais</td>
<td align="left">Neath Port Talbot</td>
<td align="left">WALES</td>
<td align="left">143000</td>
<td align="left">65000</td>
</tr>
<tr class="even">
<td align="left">Riverside</td>
<td align="left">Liverpool</td>
<td align="left">North West</td>
<td align="left">142000</td>
<td align="left">69500</td>
</tr>
<tr class="odd">
<td align="left">North Ormesby</td>
<td align="left">Middlesbrough</td>
<td align="left">North East</td>
<td align="left">70000</td>
<td align="left">36000</td>
</tr>
<tr class="even">
<td align="left">Aberdovey</td>
<td align="left">Gwynedd</td>
<td align="left">WALES</td>
<td align="left">300000</td>
<td align="left">161650</td>
</tr>
<tr class="odd">
<td align="left">Horden</td>
<td align="left">County Durham</td>
<td align="left">North East</td>
<td align="left">74000</td>
<td align="left">39950</td>
</tr>
<tr class="even">
<td align="left">Town</td>
<td align="left">Newcastle-under-Lyme</td>
<td align="left">West Midlands</td>
<td align="left">120000</td>
<td align="left">65000</td>
</tr>
</tbody>
</table>
<table style="width:99%;">
<colgroup>
<col width="27%" />
<col width="18%" />
<col width="16%" />
<col width="36%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">percentage_change</th>
<th align="left">price_diff</th>
<th align="left">X2007_inf</th>
<th align="left">perc_change_inf_adjusted</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">-54.55</td>
<td align="left">-78000</td>
<td align="left">174460</td>
<td align="left">-62.74</td>
</tr>
<tr class="even">
<td align="left">-51.06</td>
<td align="left">-72500</td>
<td align="left">173240</td>
<td align="left">-59.88</td>
</tr>
<tr class="odd">
<td align="left">-48.57</td>
<td align="left">-34000</td>
<td align="left">85400</td>
<td align="left">-57.85</td>
</tr>
<tr class="even">
<td align="left">-46.12</td>
<td align="left">-138350</td>
<td align="left">366000</td>
<td align="left">-55.83</td>
</tr>
<tr class="odd">
<td align="left">-46.01</td>
<td align="left">-34050</td>
<td align="left">90280</td>
<td align="left">-55.75</td>
</tr>
<tr class="even">
<td align="left">-45.83</td>
<td align="left">-55000</td>
<td align="left">146400</td>
<td align="left">-55.6</td>
</tr>
</tbody>
</table>
<p>So visually nothing changes in your data when you group by, however if you look at the line just before your data table above, it says <em>Groups: region [4]</em>, indicating that the data is grouped by region. The number 4 just indicates that in our current view of the data (the first six rows), there are four regions in view.</p>
<p>Now we have grouped our data, we can see the power of <code>group_by()</code> and <code>summarise()</code> working together with our next call.</p>
<pre class="r"><code>ward_data_decrease_grouped_summarise &lt;- summarise(ward_data_decrease_grouped, number_decreased = n())</code></pre>
<table style="width:62%;">
<colgroup>
<col width="37%" />
<col width="25%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">region</th>
<th align="left">number_decreased</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">East Midlands</td>
<td align="left">542</td>
</tr>
<tr class="even">
<td align="left">East of England</td>
<td align="left">207</td>
</tr>
<tr class="odd">
<td align="left">London</td>
<td align="left">3</td>
</tr>
<tr class="even">
<td align="left">North East</td>
<td align="left">305</td>
</tr>
<tr class="odd">
<td align="left">North West</td>
<td align="left">756</td>
</tr>
<tr class="even">
<td align="left">South East</td>
<td align="left">187</td>
</tr>
<tr class="odd">
<td align="left">South West</td>
<td align="left">588</td>
</tr>
<tr class="even">
<td align="left">WALES</td>
<td align="left">627</td>
</tr>
<tr class="odd">
<td align="left">West Midlands</td>
<td align="left">532</td>
</tr>
<tr class="even">
<td align="left">Yorkshire and The Humber</td>
<td align="left">409</td>
</tr>
</tbody>
</table>
<p>So the table above shows the number of wards in each region where the median price has gone down.</p>
</div>
<div id="exercises" class="section level2">
<h2>Exercises:</h2>
<ol style="list-style-type: decimal">
<li><p>Create a new dataset and include just the name of the ward name, ward code, the region and the values for 2008, 2011 and 2014.</p></li>
<li><p>Create a new dataset that will just include wards where the median house price for 2010 was above £200,000</p></li>
<li><p>Find the average mean house price for 2017 by region (Hint: You will need to use <code>group_by</code> and <code>summarise</code> functions here)</p></li>
</ol>
</div>
