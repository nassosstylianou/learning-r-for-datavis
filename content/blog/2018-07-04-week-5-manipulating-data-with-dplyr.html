---
title: "Week 5: Manipulating Data With Dplyr"
author: "Nassos"
date: "2018-07-04T10:07:55+01:00"
categories: ["R"]
tags: ["R Markdown", "ggplot2", "R"]
---



<p>Before starting this week, you should be able to load a dataset into R and examine it, as well as having had a go to turn your data from wide to long format and vice versa using <code>tidyr</code>’s <code>gather()</code> and <code>spread()</code> functions.</p>
<p>Don’t worry if you don’t feel fully comfortable with the <code>gather</code> and <code>spread</code> functions. We’ve put together a little additional exercise/example of using these two functions, which hopefully will help explain things a little better and that you can refer to whenever you need to tweak your data format to remind you how it all works.</p>
<ul>
<li>Link: <a href="http://learning-r-for-datavis.netlify.com/blog/2018-07-04-week-4-bonus-clara-s-tidyr-tutorial/">A bonus tidyr exercise</a></li>
</ul>
<div id="what-will-we-cover-this-week" class="section level2">
<h2>What will we cover this week?</h2>
<p>This week is the last one before we move into ggplot and producing graphics, so we’re almost there!</p>
<p>We will be looking at the <code>dplyr</code> package, which includes some basic but very powerful functions to help you manipulate your data. A lot of the time you will want to filter your data or just choose a few columns, or find some conclusions from your data before plotting it and these functions will help you do this.</p>
<p>You may already be used to doing this in Excel for example, adding a column that calculates percentages, for instance, or using a pivot table to find the total number of datapoints for a particular grouping in your data.</p>
<p>This week we will start by installing the <code>dplyr</code> package, which helps us manipulate and summarise our data.</p>
</div>
<div id="what-is-dplyr" class="section level2">
<h2>What is <code>dplyr</code>?</h2>
<p>Dplyr is part of a series of a grammar of data manipulation, providing a consistent set of verbs that help you solve the most common data manipulation challenges:</p>
<ul>
<li><code>select()</code>: pick columns from your data.</li>
<li><code>mutate()</code>: create new columns from calculations to your existing columns</li>
<li><code>filter()</code>: filter rows based on their value</li>
<li><code>arrange()</code>: change the ordering of the rows.</li>
<li><code>summarise()</code>: reduce multiple values down to a single summary.</li>
</ul>
<p>We’ll also look at <code>group_by()</code>, which allows for grouping data based on column values, and using the pipe <code>%&gt;%</code> operator.</p>
<p>For a visual representation of what each of the <code>dplyr</code> verbs does:</p>
<p><a href="#dplyr_verbs_visual">Visual guide to dplyr verbs</a></p>
</div>
<div id="piping" class="section level2">
<h2>Piping %&gt;%</h2>
<p>The <code>%&gt;%</code> operator allows you to extract the first argument of a function from the arguments list and put it in front of it - making writing R code much easier and avoiding repetition.</p>
<p>You can read it as ‘then’.</p>
<p>So to arrange your data by ascending order based on a column in the data, you would use the dplyr ver <code>arrange</code>. This is the standard way you would do it: <code>sorted_data &lt;- arrange(data, column_name)</code></p>
<p>You can use the pipe operator (%&gt;%) and write this instead:</p>
<p><code>sorted_data &lt;- data %&gt;% arrange(column_name)</code></p>
<p>You can also add more pipes, and the resulting dataset will be the first argument of your second line for example.</p>
<p>The pipe is originally the <code>magrittr</code> package, but will become available to you when you install and load <code>dplyr</code> into your workspace.</p>
<p>You will see the full power of <code>%&gt;%</code> is when combining multiple functions, as you will see later in your <code>dplyr</code> tutorials below.</p>
</div>
<div id="by-the-end-of-this-week" class="section level2">
<h2>By the end of this week…</h2>
<p>You should:</p>
<ul>
<li><p>Know how to select a subset of columns from your dataset</p></li>
<li><p>Know how to select a subset of rows from your dataset based on a condition</p></li>
<li><p>Order your dataset by the data in a particular column, from largest value to smallest</p></li>
<li><p>Create a new column based on an existing column in yout data</p></li>
<li><p>Use <code>group_by()</code> and <code>summarise()</code> to create results tables from your existing data</p></li>
</ul>
</div>
<div id="learning-material" class="section level2">
<h2>Learning material</h2>
<ul>
<li><p><a href="https://www.youtube.com/watch?v=jWjqLW-u3hc">[Video] Hands-on dplyr tutorial</a></p></li>
<li><p><a href="https://www.datacamp.com/courses/exploratory-data-analysis-in-r-case-study">Datacamp: Data cleaning and summarizing with dplyr</a></p></li>
</ul>
</div>
<div id="bonus-learning" class="section level2">
<h2>Bonus learning</h2>
<p>Once you have gone through the video and the Datacamp lesson, there is a tutorial that we (data team) have put together here:</p>
<p><a href="http://learning-r-for-datavis.netlify.com/blog/2018-07-04-week-5-data-team-dplyr-tutorial/" class="uri">http://learning-r-for-datavis.netlify.com/blog/2018-07-04-week-5-data-team-dplyr-tutorial/</a></p>
<p>There are a couple of exercises at the bottom which you can go through.</p>
<div id="visual-guide-to-dplyr-verbs" class="section level3">
<h3><a id="dplyr_verbs_visual"></a> Visual guide to dplyr verbs</h3>
<div id="select" class="section level4">
<h4>Select</h4>
<p><img src="/img/week5/select_dplyr.png" /><!-- --></p>
</div>
<div id="mutate" class="section level4">
<h4>Mutate</h4>
<p><img src="/img/week5/mutate_dplyr.png" /><!-- --></p>
</div>
<div id="filter" class="section level4">
<h4>Filter</h4>
<p><img src="/img/week5/filter_dplyr.png" /><!-- --></p>
</div>
<div id="summarise" class="section level4">
<h4>Summarise</h4>
<p><img src="/img/week5/summarise_dplyr.png" /><!-- --></p>
</div>
<div id="group-by" class="section level4">
<h4>Group By</h4>
<p><img src="/img/week5/group_by_dplyr.png" /><!-- --></p>
</div>
</div>
</div>
