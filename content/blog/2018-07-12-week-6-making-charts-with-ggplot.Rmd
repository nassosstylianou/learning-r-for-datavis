---
title: "Week 6: Making Charts With Ggplot"
author: "Nassos"
date: "2018-07-11T10:07:55+01:00"
categories: ["R"]
tags: ["R Markdown", "ggplot2", "R"]
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(collapse = TRUE)
```

This is the week we've all been waiting for - when we actually make charts! We will get back to what we learnt about data loading, data manipulation and analysis in R in the coming weeks as and when we need these skills to make graphics, but for this week you will be pleased to know that we will be focusing just on graphics and nothing else.

If you are still to go through `dplyr` and don't feel too comfortable with it yet, you can go through the  [tutorial we went over in class earlier in the week](http://learning-r-for-datavis.netlify.com/blog/2018-07-04-week-5-data-team-dplyr-tutorial/). But don't worry if not, as this week's class does not depend on it

##What will we cover this week?

This week is an introduction to `ggplot2`, which is R's most popular charting library and what we use to make graphics in R. 

Ggplot works based on something called 'the grammar of graphics', which is the theory behind how it works. If you are really interested in the theory behind it, you can check out [this short Datacamp video](https://www.youtube.com/watch?v=uiTc55clwuA), but how it works will become apparent when you start using it, so this is only if you are really interested in the background behind it.

##By the end of this week... you should:

* Be familiar with the basics of ggplot

* Create a really simple ggplot2 bar chart

##Learning material

So this week we are going to follow this very comprehensive `ggplot` tutorial:

http://r-statistics.co/Complete-Ggplot2-Tutorial-Part1-With-R-Code.html 

It really helps understanding the basics but mainly focuses on a scatterplot as the chart type. 

But here are some very basic follow-up tutorials you can do to draw a [line chart](https://www.r-bloggers.com/how-to-make-a-line-chart-with-ggplot2/) and a [bar chart](https://www.r-bloggers.com/make-a-bar-plot-with-ggplot/) to just get the feel for making different chart types. 

You should also follow parts [6.3 and 6.5 here for more info on bar charts](http://r-statistics.co/ggplot2-Tutorial-With-R.html)

##Exercises:

1. Make a bar chart showing the top 10 countries by life expectancy. To make your lives easier this week and to focus just on the chart-making process, the data should be downloaded here:

https://www.dropbox.com/s/inn9nw466pwl1ap/data_for_chart.csv?dl=1 

If you are feeling particularly adventurous, you could also colour each bar by continent. 

As ever, any questions just ask!
