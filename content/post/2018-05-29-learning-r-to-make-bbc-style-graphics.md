+++
title = "2018 05 29 Learning R to Make BBC Style Graphics"
date = "2018-05-29T16:28:59+01:00"
description = "About the page"
+++
This is a website built by the BBC Visual Journalism Data team that includes resources and tutorials on learning `R` to be able to create BBC-style graphics.
